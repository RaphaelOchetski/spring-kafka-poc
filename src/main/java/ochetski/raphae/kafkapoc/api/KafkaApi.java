package ochetski.raphae.kafkapoc.api;

import ochetski.raphae.kafkapoc.service.ProducerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class KafkaApi {

    private final ProducerService producerService;

    public KafkaApi(ProducerService producerService) {
        this.producerService = producerService;
    }

    @PostMapping(value = "/publish")
    public void sendMessage(@RequestParam String message) {
        producerService.sendMessage(message);
    }
}
