package ochetski.raphae.kafkapoc.service;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {

    private static final Logger logger = LoggerFactory.getLogger(ProducerConfig.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String topicName;

    public ProducerService(KafkaTemplate<String, String> kafkaTemplate, @Value("${kafka.topic-name}") String topicName) {
        this.kafkaTemplate = kafkaTemplate;
        this.topicName = topicName;
    }

    public void sendMessage(String message){
        logger.info("Producing: {}", message);
        kafkaTemplate.send(topicName, message);
    }
}
